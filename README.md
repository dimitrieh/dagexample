This project is a working example of the sample from https://docs.gitlab.com/ee/ci/yaml/#needs. Each stage will sleep a random duration from 10 to 30 seconds so you can see the out of order execution. The `rspec` and `rubocop` jobs will as soon as the corresponding `build` job is complete, and the deployment job will run only when all tests have passed.

![sample pipeline](pipeline.png "sample pipeline")

Read more about the Directed Acyclic Graph (DAG) pipeline mode at https://docs.gitlab.com/ee/ci/directed_acyclic_graph/